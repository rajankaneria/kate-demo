/* all script goes here */
 var clone;
$( function() {
    $( "#lockerSelector .locker-image" ).draggable({
    	helper: 'clone'
    });
    $("#lockerFrame").droppable({
	    	accept: ".locker-image",
            hoverClass: 'dropareahover',
            tolerance: 'pointer',
            drop: function(ev, ui)
            {
              	var dropElemId = ui.draggable.attr("id");
              	var dropElem = ui.draggable.html();
              	clone = $(dropElem).clone(); // clone it and hold onto the jquery object
                $(this).append(clone);
                var maxWidth = 0;
				$('#lockerFrame img').each(function() {
				    maxWidth = Math.max(maxWidth, parseInt($(this).width()));
				});
                console.log(maxWidth);
                $("#lockerFrame").width(maxWidth);
	    	}
    });
});